-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 07 Sie 2019, 14:43
-- Wersja serwera: 10.1.26-MariaDB
-- Wersja PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `workfromhome`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `permissions`
--

CREATE TABLE `permissions` (
  `permissions_id` int(10) NOT NULL,
  `p_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `permissions`
--

INSERT INTO `permissions` (`permissions_id`, `p_name`) VALUES
(1, 'email access granted'),
(2, 'git repository granted'),
(3, 'microsoft office licence'),
(4, 'trello access granted');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user__permissions`
--

CREATE TABLE `user__permissions` (
  `user__permissions_id` int(20) NOT NULL,
  `user_id` int(6) NOT NULL,
  `permissions_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user__report`
--

CREATE TABLE `user__report` (
  `ur_id` int(10) NOT NULL,
  `user_id` int(6) NOT NULL,
  `ur_logged` datetime NOT NULL,
  `ur_user_agent` varchar(256) CHARACTER SET latin1 NOT NULL,
  `ur_ip` varchar(20) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user__session`
--

CREATE TABLE `user__session` (
  `us_id` int(3) NOT NULL,
  `us_sess_id` varchar(32) CHARACTER SET latin1 NOT NULL,
  `us_logged` char(5) CHARACTER SET latin1 NOT NULL,
  `user_id` int(6) NOT NULL,
  `us_last_reaction` int(30) NOT NULL,
  `us_created` int(30) NOT NULL,
  `us_user_agent` varchar(256) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user__users`
--

CREATE TABLE `user__users` (
  `user_id` int(6) NOT NULL,
  `user_login` varchar(32) CHARACTER SET latin1 NOT NULL,
  `user_pswd` varchar(32) CHARACTER SET latin1 NOT NULL,
  `user_name` varchar(64) CHARACTER SET latin1 NOT NULL,
  `user_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_city` varchar(64) CHARACTER SET latin1 NOT NULL,
  `user_perm` smallint(3) NOT NULL,
  `user_source` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `user__users`
--

INSERT INTO `user__users` (`user_id`, `user_login`, `user_pswd`, `user_name`, `user_email`, `user_city`, `user_perm`, `user_source`) VALUES
(1, 'ania', 'f27f6f1c7c5cbf4e3e192e0a47b85300', 'ania b.', 'admin@admin.com', 'nekla', 9, '');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`permissions_id`);

--
-- Indexes for table `user__permissions`
--
ALTER TABLE `user__permissions`
  ADD PRIMARY KEY (`user__permissions_id`),
  ADD UNIQUE KEY `unique_index` (`user_id`,`permissions_id`);

--
-- Indexes for table `user__report`
--
ALTER TABLE `user__report`
  ADD PRIMARY KEY (`ur_id`);

--
-- Indexes for table `user__session`
--
ALTER TABLE `user__session`
  ADD PRIMARY KEY (`us_id`);

--
-- Indexes for table `user__users`
--
ALTER TABLE `user__users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `permissions`
--
ALTER TABLE `permissions`
  MODIFY `permissions_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `user__permissions`
--
ALTER TABLE `user__permissions`
  MODIFY `user__permissions_id` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `user__report`
--
ALTER TABLE `user__report`
  MODIFY `ur_id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `user__session`
--
ALTER TABLE `user__session`
  MODIFY `us_id` int(3) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `user__users`
--
ALTER TABLE `user__users`
  MODIFY `user_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3382;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
