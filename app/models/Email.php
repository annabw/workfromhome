<?php
namespace App\Models;


class Email {

    public function sendEmail($email,$subject,$fromName,$fromEmail,$message,$template_dir,$smartyArray) {

        $tpl = new \Smarty;

        $to = $email;

        $headers = '';
        $headers .= "From: ".$fromName." <".$fromEmail.">\n";
        $headers .= "MIME-Version: 1.0\n";
        $headers .= "Content-Type: text/html;\n";
        $headers .= "\tcharset=\"iso-8859-2\"\n";
        $headers .= "Content-Transfer-Encoding: 8bit\n\n";

        $pos = strpos($message,'.tpl');
        if($pos === false) {
            $message = $message;
        }
        else {
            $tpl->template_dir = $template_dir;
            foreach($smartyArray as $key => $value) {
                $tpl->assign($key,$value);
            }
            $message = $tpl->fetch($message);
        }

        mail("$to", "$subject", "$message", "$headers");
    }

}
