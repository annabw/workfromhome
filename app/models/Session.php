<?php
namespace App\Models;

use App\Core\App;

class Session {
    protected $php_session_id;
    protected $loggedIn;
    protected $userId;
    protected $sessionTimeout = 600;      // 10 minutes max time of session inactivity
    protected $sessionLifespan = 3600;    // 1 hour session's duration
    protected $strUserAgent;


    public function __construct() {

        if (isset($_COOKIE["PHPSESSID"])) {
            $phpsessid = $_COOKIE["PHPSESSID"];
            $time=time();

            try{
                App::get('database')->execute("UPDATE user__session
                                       SET us_last_reaction = '$time'
                                       WHERE us_sess_id = '$phpsessid'
                                      ");


                $userSession = App::get('database')->query("SELECT us_last_reaction, us_created
                                                           FROM user__session
                                                           WHERE us_sess_id = '$phpsessid'
                                                          ",'fetch');

                if (count($userSession) > 0)  {

                    $visited_time = $userSession["us_last_reaction"] - $userSession["us_created"];

                    if ($visited_time > $this->sessionTimeout) {

                        $ses_id = session_id();
                        if (isset($ses_id))
                        {
                            $this->logOut();
                        }
                    }
                }
            }
            catch(Exception $e){
                echo $e->getMessage();
                exit();
            }

        }

        if(session_status() <> 2) {
            session_set_cookie_params($this->sessionLifespan);
            session_start();
        }

    }

    public function authentication() {
        $ses_id = session_id();

        if (isset($_SESSION['user_id']) && isset($_SESSION['agent']) && isset($ses_id)) {
            if ($_SESSION['agent'] <> $_SERVER['HTTP_USER_AGENT']) {
                return $this->LogOut();
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    public function login($strEmail, $strPlainPassword) {
        global $_SESSION;
        $this->strUserAgent = $_SERVER["HTTP_USER_AGENT"];
        $ip = $_SERVER['REMOTE_ADDR'];
        $time=time();

        $strMD5Password = md5($strPlainPassword);

        try{
            $user = App::get('database')->query("SELECT user_id, user_perm FROM user__users
                       WHERE user_email = '$strEmail'
                       AND user_pswd = '$strMD5Password'
                      ",'fetch');

            $this->php_session_id = $this->GetSessionIdentifier();
            if ($user <> false)  {
                $this->loggedIn = true;
                $_SESSION['user_id'] = $user["user_id"];
                $_SESSION['agent'] = $this->strUserAgent;
                $_SESSION['perm'] = $user["user_perm"];

                App::get('database')->execute("INSERT INTO user__session (us_sess_id, us_logged, user_id, us_last_reaction, us_created, us_user_agent)
                          VALUES ('$this->php_session_id', true, '".$_SESSION['user_id']."', '$time', '$time', '$this->strUserAgent')
                         ");

                App::get('database')->execute("INSERT INTO user__report (user_id, ur_logged, ur_user_agent, ur_ip)
                          VALUES ('".$_SESSION['user_id']."', now(), '$this->strUserAgent', '$ip')
                         ");
                return true;
            }
            else
            {
                return false;
            };
        }
        catch(Exception $e){
            echo $e->getMessage();
            exit();
        }
    }


    public function logOut() {

        //destroying session, session's variables and cookie
        if(session_status() == 2) {
            $_SESSION = array();

            if (isset($_COOKIE[session_name()])) {
                setcookie(session_name(), '', time()-42000, '/');
            }
            session_destroy();
            return true;
        }
    }

    public function getSessionIdentifier() {
        $this->php_session_id = session_id();
        return($this->php_session_id);
    }
}
?>
