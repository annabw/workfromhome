<?php
namespace App\Models;

use App\Core\App;

class Permission {

    public function getAllPermissions() {

        return App::get('database')->selectAll('permissions');
    }

    public function addUserPermissions($data, $user_id) {

        foreach($data as $perm)
        {
            App::get('database')->insert('user__permissions', [
                'user_id' => $user_id,
                'permissions_id' => $perm
            ]);
        }
        return true;
    }

    public function getUserPermissions($id) {
        try{
            $perm = App::get('database')->query("
                                select c.p_name
                                from
                                    user__users a
                                        inner join
                                    user__permissions 	 b
                                        on a.user_id = b.user_id
                                        inner join 
                                    permissions c
                                        on b.permissions_id = c.permissions_id
                                where a.user_id = '{$id}'          
                       ", 'fetchAll');
            return $perm;
        }
        catch(Exception $e){
            echo $e->getMessage();
            exit();
        }
    }
}
