<?php
namespace App\Models;

use App\Core\App;

class User {

    protected $auth;

    public function __construct()
    {
        $this->auth = new Session();
    }

    public function checkUserAdmin()
    {
        // checking, if user is an admin
        if ($this->auth->authentication() === true) {
            if (isset($_SESSION['perm']) && $_SESSION['perm'] == '9') return true;
        }
        return false;
    }

    public static function getUserPerm() {
        return $_SESSION['perm'];
    }

    public static function getUserName() {

        if (isset($_SESSION['user_id'])) {
            try{
                $user = App::get('database')->query("SELECT user_name FROM user__users
                       WHERE user_id = ".$_SESSION['user_id']."
                       ", 'fetch');
                return $user['user_name'];
            }
            catch(Exception $e){
                echo $e->getMessage();
                exit();
            }
        }
    }

    public function addUser($data)
    {
        $user = App::get('database')->insert('user__users', [
            'user_login' => getValue($data['u_email']),
            'user_pswd' => $data['password'],
            'user_name' => getValue($data['name']),
            'user_email' => getValue($data['u_email']),
            'user_city' => getValue($data['city']),
            'user_perm' => 3
        ]);

        if($user <> null) {
            return $user;
        }
        return false;
    }

    public function getUserDetails($id) {
        try{
            $user = App::get('database')->query("SELECT * FROM user__users
                       WHERE user_id = ".$id."
                       ", 'fetch');
            return $user;
        }
        catch(Exception $e){
            echo $e->getMessage();
            exit();
        }
    }
}
