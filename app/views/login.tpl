{include file='app/views/layout/header.tpl'}
<section class="account-action-page">
    <div class="row">
        <div class="large-5 large-offset-3 medium-7 medium-offset-2 small-12 columns margin-top-60px">
            <h1 class="">Log in</h1>
            <form novalidate>
                <div class="row">
                    <div class="small-12 columns">
                        <input required name="u_email" type="email"  placeholder="E-mail" class="big-inputs label_better" data-position="left" value="" id="u_email">
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 columns">
                        <input required name="u_password" placeholder="Password" class="big-inputs label_better login-field login-field-password " data-position="left" id="u_password" type="password" value="" data-txt-hide="Hide password" data-txt-show="Show password">
                    </div>
                </div>

                <div class="row">
                    <div class="medium-6 columns">
                        <input type="hidden" name="token" value="{$token}" id="token">
                        <button type="submit" class="button success radius full-width" id="form-button-submit">Log in</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

{literal}
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <script src="sweetalert2.min.js"></script>


    <script type="text/javascript">
        $('document').ready(function () {
            $('#form-button-submit').on('click', function(e){
                e.preventDefault();

                var jsonData = '';

                jsonData = { "u_email":$('#u_email').val(),
                            "u_password":$('#u_password').val(),
                            "token":$('#token').val()};


                $.ajax({
                    url: '/logcheck',
                    type: 'post',
                    dataType: 'json',
                    data: jsonData,
                    dataType: 'html',
                    success: function (data) {
                        var resultData = JSON.parse(data);

                        if (typeof resultData.contents.error !== 'undefined') {
                            Swal.fire({
                                title: 'Upps',
                                text: resultData.contents.error,
                                type: 'error'
                            })
                        }
                        else {
                            window.location.href = "/employees";
                        }

                    }
                });
            });
        });
    </script>
{/literal}

{include file='app/views/layout/footer.tpl'}