<?php
/* Smarty version 3.1.33, created on 2019-08-07 13:25:37
  from 'C:\xampp\htdocs\app\modules\workfromhome\views\employee\create.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d4ab5314dafb5_89373054',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'af887c5c4aa74f1db18fb33dfd205ab288ffc08f' => 
    array (
      0 => 'C:\\xampp\\htdocs\\app\\modules\\workfromhome\\views\\employee\\create.tpl',
      1 => 1565177123,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:app/views/layout/header.tpl' => 1,
    'file:app/views/layout/footer.tpl' => 1,
  ),
),false)) {
function content_5d4ab5314dafb5_89373054 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:app/views/layout/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<span class="logout"><?php if (isset($_smarty_tpl->tpl_vars['u_name']->value)) {?> <a href="/logout"> Log out <?php echo $_smarty_tpl->tpl_vars['u_name']->value;?>
</a> <?php }?></span>
<section class="account-action-page">
    <div class="row">
        <div class="large-5 large-offset-3 medium-7 medium-offset-2 small-12 columns margin-top-60px">
            <h1 class="">Add an employee</h1>

            <form novalidate>
                <div class="form-field">
                    <input required name="name" type="input"  placeholder="Name" class="big-inputs label_better" data-position="left" value="" id="name">
                    <span class="error-msg" id="errorName"></span>
                </div>
                <div class="form-field">
                    <input required name="u_email" type="email"  placeholder="E-mail" class="big-inputs label_better" data-position="left" value="" id="u_email">
                    <span class="error-msg" id="errorEmail"></span>
                </div>

                <div class="form-field">
                    <select name="city" data-position="left" value="" id="city" class="big-inputs">
                        <option value="" disabled selected>Select office</option>
                        <option value="Barcelona">Barcelona</option>
                        <option value="Malta">Malta</option>
                        <option value="Remote">Remote</option>
                    </select>
                </div>

                <div class="form-field">
                    <div id="perm">
                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['permissions']->value, 'i');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value) {
?>
                            <label class="wfhcontainer"><?php echo $_smarty_tpl->tpl_vars['i']->value->p_name;?>

                                <input type="checkbox" name="perm" value="<?php echo $_smarty_tpl->tpl_vars['i']->value->permissions_id;?>
">
                                <span class="checkmark"></span>
                            </label>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                    </div>
                </div>

                <div class="row">
                    <div class="medium-6 columns">
                        <input type="hidden" name="token" value="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
">
                        <button type="submit" class="button success radius full-width" id="form-button-submit">Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>




    <?php echo '<script'; ?>
 type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="sweetalert2.min.js"><?php echo '</script'; ?>
>


    <?php echo '<script'; ?>
 type="text/javascript">
    $('document').ready(function () {
        $('#form-button-submit').on('click', function(e){

            e.preventDefault();
            var jsonData = '';

            var allPermVals = [];
            $('#perm :checked').each(function() {
                allPermVals.push($(this).val());
            });

            jsonData = { "name":$('#name').val(),
                         "u_email":$('#u_email').val(),
                         "city":$('#city').val(),
                         "perm":allPermVals};

            $('#errorName').html('');
            $('#errorEmail').html('');

            $.ajax({
                url: '/employees/store',
                type: 'post',
                dataType: 'json',
                data: jsonData,
                dataType: 'html',
                success: function (data) {
                    var resultData = JSON.parse(data);
                    //console.log(resultData);
                    $('#errorName').append(resultData.contents.errors.name);
                    if(resultData.contents.errors.email != '') {
                        $('#errorEmail').append(resultData.contents.errors.email);
                    }
                    else if (resultData.contents.errors.validEmail != '') {
                        $('#errorEmail').append(resultData.contents.errors.validEmail);
                    }
                    else {
                        $('#errorEmail').append(resultData.contents.errors.unigueEmail);
                    }

                    if (typeof resultData.contents.successMsg !== 'undefined') {
                        Swal.fire({
                            title: 'Success',
                            text: resultData.contents.successMsg,
                            type: 'success',
                            showCancelButton: false,
                            showConfirmButton: false
                        })

                        setTimeout(function () {
                            window.location.href = "/employees";
                        }, 3000);
                    }
                }
            });
        });
    });
    <?php echo '</script'; ?>
>

<?php $_smarty_tpl->_subTemplateRender('file:app/views/layout/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
