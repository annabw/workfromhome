<?php
/* Smarty version 3.1.33, created on 2019-08-07 14:40:57
  from 'C:\xampp\htdocs\app\modules\workfromhome\views\employee\details.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d4ac6d92b3ce4_75874137',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8b99e8084ea3350288b15a8d3c14fb04e9495d81' => 
    array (
      0 => 'C:\\xampp\\htdocs\\app\\modules\\workfromhome\\views\\employee\\details.tpl',
      1 => 1565181653,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:app/views/layout/header.tpl' => 1,
    'file:app/views/layout/footer.tpl' => 1,
  ),
),false)) {
function content_5d4ac6d92b3ce4_75874137 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:app/views/layout/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<span class="logout"><?php if (isset($_smarty_tpl->tpl_vars['u_name']->value)) {?> <a href="/logout"> Log out <?php echo $_smarty_tpl->tpl_vars['u_name']->value;?>
</a> <?php }?></span>

<div class="title-content">
    <div class="row">
        <div class="large-6 medium-6 columns">

            <h1><?php echo $_smarty_tpl->tpl_vars['user']->value['user_name'];?>
</h1>
            <p class="subheadline"><?php echo $_smarty_tpl->tpl_vars['user']->value['user_email'];?>
</p>

            <div>
                Location: <b><?php echo $_smarty_tpl->tpl_vars['user']->value['user_city'];?>
</b> <br />
            </div>
        </div>
        <div class="medium-5 large-5 columns text-center">
           <h3>Permissions:</h3>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['permissions']->value, 'i');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value) {
?>
                    &bull; <?php echo $_smarty_tpl->tpl_vars['i']->value[0];?>
 <br />
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            <br />
            <a href="/employees">Go back to Employees</a>
        </div>
    </div>

</div>



<?php $_smarty_tpl->_subTemplateRender('file:app/views/layout/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
