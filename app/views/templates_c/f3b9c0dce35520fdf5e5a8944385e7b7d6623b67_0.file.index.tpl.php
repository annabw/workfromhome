<?php
/* Smarty version 3.1.33, created on 2019-08-07 12:05:43
  from 'C:\xampp\htdocs\app\modules\workfromhome\views\employee\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d4aa2776c02c1_14601463',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f3b9c0dce35520fdf5e5a8944385e7b7d6623b67' => 
    array (
      0 => 'C:\\xampp\\htdocs\\app\\modules\\workfromhome\\views\\employee\\index.tpl',
      1 => 1565172254,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:app/views/layout/header.tpl' => 1,
    'file:app/views/layout/footer.tpl' => 1,
  ),
),false)) {
function content_5d4aa2776c02c1_14601463 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:app/views/layout/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<span class="logout"><?php if (isset($_smarty_tpl->tpl_vars['u_name']->value)) {?> <a href="/logout"> Log out <?php echo $_smarty_tpl->tpl_vars['u_name']->value;?>
</a> <?php }?></span>
<div class="page-section-content">

    <div class="row">
        <div class="large-12 columns">
            <a class="wfh-button" href="/employees/create">Add new employee</a>
        </div>
    </div>

    <div class="row">
        <div class="large-12 columns text-center">
            <a id="table"></a>
            <table class="features-table">
                <tr>
                    <th class="section">Name</th>

                    <th class="no-padding"><span class="premium-header">Details</span></th>
                    <th class="no-padding"><span class="business-header">Requests</span></th>
                </tr>

                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['users']->value, 'i');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['i']->value) {
?>
                    <tr>
                        <th><?php echo $_smarty_tpl->tpl_vars['i']->value->user_name;?>
</th>

                        <td><a href="/employees/details?id=<?php echo $_smarty_tpl->tpl_vars['i']->value->user_id;?>
">&#10132;</a></td>
                        <td></td>
                    </tr>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </table>
        </div>
    </div>
</div>


<?php $_smarty_tpl->_subTemplateRender('file:app/views/layout/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
