<?php
/* Smarty version 3.1.33, created on 2019-08-07 11:33:08
  from 'C:\xampp\htdocs\app\views\login.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d4a9ad4638270_80157223',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0ffd6628a9834acb474798010600135952a5815e' => 
    array (
      0 => 'C:\\xampp\\htdocs\\app\\views\\login.tpl',
      1 => 1565169144,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:app/views/layout/header.tpl' => 1,
    'file:app/views/layout/footer.tpl' => 1,
  ),
),false)) {
function content_5d4a9ad4638270_80157223 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:app/views/layout/header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<section class="account-action-page">
    <div class="row">
        <div class="large-5 large-offset-3 medium-7 medium-offset-2 small-12 columns margin-top-60px">
            <h1 class="">Log in</h1>
            <form novalidate>
                <div class="row">
                    <div class="small-12 columns">
                        <input required name="u_email" type="email"  placeholder="E-mail" class="big-inputs label_better" data-position="left" value="" id="u_email">
                    </div>
                </div>
                <div class="row">
                    <div class="small-12 columns">
                        <input required name="u_password" placeholder="Password" class="big-inputs label_better login-field login-field-password " data-position="left" id="u_password" type="password" value="" data-txt-hide="Hide password" data-txt-show="Show password">
                    </div>
                </div>

                <div class="row">
                    <div class="medium-6 columns">
                        <input type="hidden" name="token" value="<?php echo $_smarty_tpl->tpl_vars['token']->value;?>
" id="token">
                        <button type="submit" class="button success radius full-width" id="form-button-submit">Log in</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>


    <?php echo '<script'; ?>
 type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="sweetalert2.min.js"><?php echo '</script'; ?>
>


    <?php echo '<script'; ?>
 type="text/javascript">
        $('document').ready(function () {
            $('#form-button-submit').on('click', function(e){
                e.preventDefault();

                var jsonData = '';

                jsonData = { "u_email":$('#u_email').val(),
                            "u_password":$('#u_password').val(),
                            "token":$('#token').val()};


                $.ajax({
                    url: '/logcheck',
                    type: 'post',
                    dataType: 'json',
                    data: jsonData,
                    dataType: 'html',
                    success: function (data) {
                        var resultData = JSON.parse(data);

                        if (typeof resultData.contents.error !== 'undefined') {
                            Swal.fire({
                                title: 'Upps',
                                text: resultData.contents.error,
                                type: 'error'
                            })
                        }
                        else {
                            window.location.href = "/employees";
                        }

                    }
                });
            });
        });
    <?php echo '</script'; ?>
>


<?php $_smarty_tpl->_subTemplateRender('file:app/views/layout/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
