<!doctype html>
<html class="no-js" lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>work from home</title>
    <meta name="title" content="Rejestracja - interankiety" />
    <meta name="keywords" content="ankiety, formularze, badania online, darmowe ankiety">

    <link rel="stylesheet" type="text/css" media="screen" href="/public/css/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
</head>


<body>


<section class="account-action-page">
    <div class="top-text-bar">
        <div class="small-12 columns">
            <div class="logo-top-left">
                <a href="/employees"><img src="https://www.blexr.com/wp-content/themes/blexr/dist/images/blexr-logo_b886c21a.svg"></a>
                <span>work from home</span>
            </div>
        </div>
    </div>
</section>


<hr />