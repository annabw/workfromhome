<?php

namespace App\Controllers;

use App\Models\Session;
use App\Models\User;

class SessionController extends AppController {

    protected $session;
    protected $user;

    public function __construct()
    {
        parent::__construct();
        $this->session = new Session;
        $this->user = new User();
    }

    public function index()
    {
        if (empty($_SESSION['token'])) {
            $_SESSION['token'] = bin2hex(random_bytes(32));
        }
        $token = $_SESSION['token'];
        $this->tpl->assign("token", $token);
        $this->tpl->display('views/login.tpl');
    }

    public function show()
    {
        $result =[];
        if (!empty($_POST['token'])) {
            if (hash_equals($_SESSION['token'], $_POST['token'])) {

                $uEmail = getValue($_POST['u_email']);
                $uPswd = getValue($_POST['u_password']);
                $this->session->login($uEmail, $uPswd);
                $admin = $this->user->checkUserAdmin();

                if(!$admin)
                {
                    $result['error'] = "Authorization Failed";
                }
                else
                {
                    $result['success'] = true;

                }
            }
            else {
                throw new \Exception(
                    "Invalid action"
                );
            }
        }
        echo json_encode(['contents'=>$result]);
    }

    public function destroy()
    {
        $this->session->logOut();
        $this->tpl->assign("message", 'Thank you for using Blexr work from home app');
        $this->tpl->display('views/index.tpl');
    }
}