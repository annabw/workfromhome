<?php

namespace App\Controllers;

class AppController
{
    protected $tpl;

    public function __construct(){
        $this->tpl = new \Smarty;
        $this->tpl->setTemplateDir('app/');
        $this->tpl->setCompileDir('app/views/templates_c');
    }

    public static function redirectToEmployees() {
        redirect("employees");
    }
}