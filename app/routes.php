<?php  

$router->get('login', 'App\Controllers\SessionController@index');
$router->post('logcheck', 'App\Controllers\SessionController@show');
$router->get('logout', 'App\Controllers\SessionController@destroy');
$router->get('', 'App\Controllers\AppController@redirectToEmployees');