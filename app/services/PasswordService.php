<?php
namespace App\Services;

use App\Core\App;

class PasswordService {

    public static function generatePassword($number) {
        $data = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
        return substr(str_shuffle($data), 0, $number);
    }
}
