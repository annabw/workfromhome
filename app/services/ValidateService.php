<?php
namespace App\Services;

use App\Core\App;

class ValidateService {

    public static function valueEmpty($value) {
        if($value[0] == '') return "Field {$value[1]} can't be empty";
        return false;
    }

    public static function validateEmail($value) {
        if (!filter_var($value, FILTER_VALIDATE_EMAIL)) return "Invalid email format";
        return false;
    }

    public static function unigueEmail($email, $table) {
        $user = App::get('database')->query("SELECT * FROM $table
                       WHERE user_email = '$email'", 'fetch');

        if($user <> false)  return "This email already exists";
        return false;
    }

    public static function checkIfAnyErrors($error) {

        $errors = [];
        foreach($error as $item) {
            if($item <> '') $errors[] = $item;
        }
        return $errors;
    }
}
