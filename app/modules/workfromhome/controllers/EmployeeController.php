<?php

namespace App\Modules\WorkFromHome\Controllers;

use App\Core\App;
use App\Controllers\AppController;
use App\Models\User;
use App\Models\Permission;
use App\Services\ValidateService;
use App\Models\Email;
use App\Services\PasswordService;

class EmployeeController extends AppController{

    protected $user;
    protected $admin;
    protected $permission;

    public function __construct()
    {
        parent::__construct();

        $this->user = new User;
        $this->admin = $this->user->checkUserAdmin();
        $u_name = $this->user->getUserName();
        $this->tpl->assign("u_name", $u_name);

        $this->permission = new Permission;
    }

    public function index() {

        if($this->admin) {
            $users = App::get('database')->selectAll('user__users');

            $this->tpl->assign("users", $users);
            $this->tpl->display('modules/workfromhome/views/employee/index.tpl');
        }
        else {
            redirect('login');
        }
    }

    public function show() {

        if($this->admin) {
            $user = $this->user->getUserDetails(getValue($_GET['id']));
            $permissions = $this->permission->getUserPermissions($user['user_id']);
            $this->tpl->assign("user", $user);
            $this->tpl->assign("permissions", $permissions);
            $this->tpl->display('modules/workfromhome/views/employee/details.tpl');
        }
        else {
            redirect('login');
        }
    }

    public function create() {

        if($this->admin) {
            if (empty($_SESSION['token'])) {
                $_SESSION['token'] = bin2hex(random_bytes(32));
            }

            $token = $_SESSION['token'];
            $this->tpl->assign("token", $token);
            $permissions = $this->permission->getAllPermissions();
            $this->tpl->assign("permissions", $permissions);
            $this->tpl->display('modules/workfromhome/views/employee/create.tpl');
        }
        else {
            redirect('login');
        }
    }

    public function store()
    {
        $email = getValue($_POST['u_email']);
        $result['errors']['name'] = ValidateService::valueEmpty([getValue($_POST['name']), 'name']);
        $result['errors']['email'] = ValidateService::valueEmpty([$email, 'email']);
        $result['errors']['validEmail'] = ValidateService::validateEmail(getValue($_POST['u_email']));
        $result['errors']['unigueEmail'] = ValidateService::unigueEmail($email, 'user__users');

        if(count(ValidateService::checkIfAnyErrors($result['errors'])) == 0 ) {
            $smartyArray['email'] = $email;
            $smartyArray['password'] = PasswordService::generatePassword(5);
            $_POST['password'] = md5($smartyArray['password']);
            $user_id = $this->user->addUser($_POST);

            if(isset($_POST['perm'])) {
                if((($_POST['perm']) > 0) && (!is_null($user_id))) {
                    $this->permission->addUserPermissions($_POST['perm'], $user_id);
                }
            }

            Email::sendEmail($email,'Welcome to Blexr','blexr.com','info@blexr.com','email.tpl','app/modules/workfromhome/views/employee',$smartyArray);
            $result['successMsg'] = 'User successfully added';
        }

        echo json_encode(['contents'=>$result]);
    }
}
