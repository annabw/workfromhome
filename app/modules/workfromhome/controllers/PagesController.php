<?php

namespace App\Modules\WorkFromHome\Controllers;

use App\Core\App;
use App\Controllers\AppController;
use App\Models\Session;
use App\Models\User;

class PagesController extends AppController{

    public function home()
    {
        $users = App::get('database')->selectAll('user__users');

        $this->tpl->assign("users", $users);

        $this->tpl->display('modules/workfromhome/views/index.tpl');
    }

    public function about()
    {
        $session = new Session();
        $auth = $session->authentication();


        if(($auth === true)) {
            $perm = User::getUserPerm();
            if($perm == 9) {
                $param = (!empty($_GET['id']) ? $_GET['id'] : 0);
                $this->tpl->assign("name", "This is about us page");
            }
        }
        else {
            $this->tpl->assign("name", "You dont have permission to view all the content");
        }
        $this->tpl->display('modules/workfromhome/views/about.tpl');
    }

    public function add_name()
    {
        App::get('database')->insert('user__users', [
            'user_name' => $_POST['name']
        ]);

        redirect('');
    }
}