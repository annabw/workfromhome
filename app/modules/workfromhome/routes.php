<?php

$router->get('employees', 'App\Modules\WorkFromHome\Controllers\EmployeeController@index');
$router->get('employees/create', 'App\Modules\WorkFromHome\Controllers\EmployeeController@create');
$router->get('employees/details', 'App\Modules\WorkFromHome\Controllers\EmployeeController@show');
$router->post('employees/store', 'App\Modules\WorkFromHome\Controllers\EmployeeController@store');

