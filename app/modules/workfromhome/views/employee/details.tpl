{include file='app/views/layout/header.tpl'}
<span class="logout">{if isset($u_name)} <a href="/logout"> Log out {$u_name}</a> {/if}</span>

<div class="title-content">
    <div class="row">
        <div class="large-6 medium-6 columns">

            <h1>{$user.user_name}</h1>
            <p class="subheadline">{$user.user_email}</p>

            <div>
                Location: <b>{$user.user_city}</b> <br />
            </div>
        </div>
        <div class="medium-5 large-5 columns text-center">
           <h3>Permissions:</h3>
                {foreach from=$permissions item=i}
                    &bull; {$i.0} <br />
                {/foreach}
            <br />
            <a href="/employees">Go back to Employees</a>
        </div>
    </div>

</div>



{include file='app/views/layout/footer.tpl'}