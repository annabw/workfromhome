{include file='app/views/layout/header.tpl'}
<span class="logout">{if isset($u_name)} <a href="/logout"> Log out {$u_name}</a> {/if}</span>
<section class="account-action-page">
    <div class="row">
        <div class="large-5 large-offset-3 medium-7 medium-offset-2 small-12 columns margin-top-60px">
            <h1 class="">Add an employee</h1>
            <span class="error-msg" id="errorName"></span>
            <span class="error-msg" id="errorEmail"></span>
            <form novalidate>
                <div class="form-field">
                    <input required name="name" type="input"  placeholder="Name" class="big-inputs label_better" data-position="left" value="" id="name">
                </div>
                <div class="form-field">
                    <input required name="u_email" type="email"  placeholder="E-mail" class="big-inputs label_better" data-position="left" value="" id="u_email">
                </div>

                <div class="form-field">
                    <select name="city" data-position="left" value="" id="city" class="big-inputs">
                        <option value="" disabled selected>Select office</option>
                        <option value="Barcelona">Barcelona</option>
                        <option value="Malta">Malta</option>
                        <option value="Remote">Remote</option>
                    </select>
                </div>

                <div class="form-field">
                    <div id="perm">
                        {foreach from=$permissions item=i}
                            <label class="wfhcontainer">{$i->p_name}
                                <input type="checkbox" name="perm" value="{$i->permissions_id}">
                                <span class="checkmark"></span>
                            </label>
                        {/foreach}
                    </div>
                </div>

                <div class="row">
                    <div class="medium-6 columns">
                        <input type="hidden" name="token" value="{$token}">
                        <button type="submit" class="button success radius full-width" id="form-button-submit">Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>



{literal}
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
    <script src="sweetalert2.min.js"></script>


    <script type="text/javascript">
    $('document').ready(function () {
        $('#form-button-submit').on('click', function(e){

            e.preventDefault();
            var jsonData = '';

            var allPermVals = [];
            $('#perm :checked').each(function() {
                allPermVals.push($(this).val());
            });

            jsonData = { "name":$('#name').val(),
                         "u_email":$('#u_email').val(),
                         "city":$('#city').val(),
                         "perm":allPermVals};

            $('#errorName').html('');
            $('#errorEmail').html('');

            $.ajax({
                url: '/employees/store',
                type: 'post',
                dataType: 'json',
                data: jsonData,
                dataType: 'html',
                success: function (data) {
                    var resultData = JSON.parse(data);
                    //console.log(resultData);
                    $('#errorName').append(resultData.contents.errors.name + '<br />');
                    if(resultData.contents.errors.email != '') {
                        $('#errorEmail').append(resultData.contents.errors.email + '<br />');
                    }
                    else if (resultData.contents.errors.validEmail != '') {
                        $('#errorEmail').append(resultData.contents.errors.validEmail + '<br />');
                    }
                    else {
                        $('#errorEmail').append(resultData.contents.errors.unigueEmail + '<br />');
                    }

                    if (typeof resultData.contents.successMsg !== 'undefined') {
                        Swal.fire({
                            title: 'Success',
                            text: resultData.contents.successMsg,
                            type: 'success',
                            showCancelButton: false,
                            showConfirmButton: false
                        })

                        setTimeout(function () {
                            window.location.href = "/employees";
                        }, 3000);
                    }
                }
            });
        });
    });
    </script>
{/literal}
{include file='app/views/layout/footer.tpl'}