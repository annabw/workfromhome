{include file='app/views/layout/header.tpl'}
<span class="logout">{if isset($u_name)} <a href="/logout"> Log out {$u_name}</a> {/if}</span>
<div class="page-section-content">

    <div class="row">
        <div class="large-12 columns">
            <a class="wfh-button" href="/employees/create">Add new employee</a>
        </div>
    </div>

    <div class="row">
        <div class="large-12 columns text-center">
            <a id="table"></a>
            <table class="features-table">
                <tr>
                    <th class="section">Name</th>

                    <th class="no-padding"><span class="premium-header">Details</span></th>
                    <th class="no-padding"><span class="business-header">Requests</span></th>
                </tr>

                {foreach from=$users item=i}
                    <tr>
                        <th>{$i->user_name}</th>

                        <td><a href="/employees/details?id={$i->user_id}">&#10132;</a></td>
                        <td></td>
                    </tr>
                {/foreach}
            </table>
        </div>
    </div>
</div>


{include file='app/views/layout/footer.tpl'}