<?php   

$router->get('login', 'App\Controllers\SessionController@index');
$router->post('logcheck', 'App\Controllers\SessionController@show');
$router->get('logout', 'App\Controllers\SessionController@destroy');
$router->get('', 'App\Controllers\AppController@redirectToEmployees');

$router->get('employees', 'App\Modules\WorkFromHome\Controllers\EmployeeController@index');
$router->get('employees/create', 'App\Modules\WorkFromHome\Controllers\EmployeeController@create');
$router->get('employees/details', 'App\Modules\WorkFromHome\Controllers\EmployeeController@show');
$router->post('employees/store', 'App\Modules\WorkFromHome\Controllers\EmployeeController@store');

