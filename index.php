<?php
ob_start();
require 'vendor/autoload.php';
require 'core/bootstrap.php';

use App\Core\Router;
use App\Core\Request;

$modulesRoutes = ["app/routes.php", "app/modules/workfromhome/routes.php"];

//generating routes from all location
$files = '';
foreach ($modulesRoutes as $route) {
    $files .= file_get_contents($route);
}
$file = str_replace('<?php', '', $files);
$file = '<?php '.$file;

$myfile = fopen("routes.php", "w") or die("Unable to open file!");
fwrite($myfile, '');
fwrite($myfile, $file);

Router::load("routes.php")
    ->direct(Request::uri(), Request::method());

ob_end_flush();