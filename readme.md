# Welcome to Work From Home app 

## Information about app


* schemas in main folder workfromhome.sql 

* change config.php 

* life working app on [workfromhome.hekko24.pl](http://workfromhome.hekko24.pl/) (login: admin@admin.com,  password: password)

## What has been done:

* Adding new users: When new employees are hired, the administrator must have a screen to add new employees. An email should be sent out with the employee's log in details and random password. 

* User enabled features: each employee is to have a set of checkboxes which the administrator is to tick off when they have been completed. These tick boxes include: "email access granted", "git repository granted", "microsoft office licence", "trello access granted". These are just to keep track of what licenses have been given to the employee.

* Make use of MySql, PHP and javascript

* Multiple office locations

* Making use of jQuery and AJAX

```php
Note: The best code comment is often no comment at all. We should name variables and funtions to read code easly.     
```


