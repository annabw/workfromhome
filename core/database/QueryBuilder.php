<?php

class QueryBuilder
{
    protected $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function selectAll($table)
    {
        $statement = $this->pdo->prepare("select * from {$table}");

        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_CLASS);
    }

    public function selectRecord($table, $fields, $condition)
    {
        if($condition <> '') {
            $condition = 'and '.$condition;
        }
        $statement = $this->pdo->prepare("select {$fields} from {$table} where 1 {$condition}");
        $statement->execute();

        return $statement->fetchAll(PDO::FETCH_CLASS);
    }

    public function query($query, $operation)
    {
        $statement = $this->pdo->prepare($query);
        $statement->execute();
        return $statement->{$operation}();
    }


    public function execute($query)
    {
        $statement = $this->pdo->prepare($query);
        return $statement->execute();
    }

    public function insert($table, $parameters)
    {
        $sql = sprintf(
            'insert into %s (%s) values (%s)',
            $table,
            implode(', ', array_keys($parameters)),
            ':' . implode(', :', array_keys($parameters))
        );

        try {
            $statement = $this->pdo->prepare($sql);

            $statement->execute($parameters);

            return $this->pdo->lastInsertId();

        } catch (Exception $e) {
            //
        }
    }

    public function getRecord($tablesInOrder,$fields,$condition,$connection_param) {

        $arrTables = explode(',',$tablesInOrder);
        $join = '';

        if($connection_param == '')
        {
            for ($i = 1; $i <= count($arrTables)-1; $i++) {
                $join .= ' JOIN '.$arrTables[$i].' ON '.$arrTables[$i-1].'.'.$arrTables[$i].'_id = '.$arrTables[$i].'.'.$arrTables[$i].'_id ';
            }
        }
        else
        {
            for ($i = 1; $i <= count($arrTables)-1; $i++) {
                $join .=  ' JOIN '.$arrTables[$i].' ON '.$arrTables[$i-1].'.'.$connection_param.' = '.$arrTables[$i].'.'.$connection_param.' ';
            }
        }

        if($condition <> false) {
            $condition = "AND ".$condition;
        }

        try{
            $statement = $this->pdo->query("SELECT $fields
					FROM $arrTables[0] 
					$join
					WHERE 1
					$condition
                    ");

            return $statement->fetch();
        }
        catch(Exception $e){
            echo $e->getMessage();
            exit();
        }
    }
}