<?php

use App\Core\App;

App::bind('config', require 'config.php');
App::bind('database', new QueryBuilder( Connection::make(App::get('config')['database'])));


function redirect($path)
{
    header("Location: /{$path}");
}


function dd()
{
    echo '<pre>';
    array_map(function($x) { var_dump($x); }, func_get_args());
    die;
}

function getValue($field) {
    $field = trim($field);
    if (get_magic_quotes_gpc()) $field = stripslashes($field);
    return str_replace(array("&" , '"' , "<" ,">" , "\0", "\\" , "'"), // from
        array("&amp;", "&quot;", "&lt;","&gt;", "" , "\\\\", "\'" ), // to
        $field
    );
}